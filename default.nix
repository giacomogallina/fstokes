with import <nixpkgs> {};
let buildInputs = [
    python310
    python310Packages.numpy
    python310Packages.matplotlib
    gfortran
    gcc
    lapack
    fortran-fpm
];
in stdenv.mkDerivation {
    name = "build-environment"; # Probably put a more meaningful name here
    buildInputs = buildInputs;
    nativeBuildInputs = [];
    LD_LIBRARY_PATH="${lib.makeLibraryPath buildInputs}";
}
