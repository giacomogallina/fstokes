# `fstokes`
A simple numerical solver for the steady 2D Stokes problem,
written in Fortran 2008 by Giacomo Gallina

## Building `fstokes`
`fstokes` needs the following dependencies:
- a Fortran compiler
- a C compiler
- either the [Fortran Package Manager](https://fpm.fortran-lang.org/) or GNU Make
- (optional) python3 with numpy and matplotlib, used for plotting

I only tested the combination gfortran + gcc, so
I won't make guarantees on any other setup.
Once you have installed all of the above, you can choose between compiling with `fpm`
or with `make`.

#### `fpm`
```
$ fpm build
```
You can also pass the `--profile release` option to `fpm`,
in order to compile with extra optimizations.

#### `make`
```
$ make
```

## Running `fstokes`
`fpm` also lets you run the program you just compiled, like so:
```
$ fpm run
```
To run the optimized version, you need to add the `--profile release` flag.
However, `fstokes` requires some command line arguments, so the
above will just give an error and terminate. To pass command
line arguments to `fstokes`, and not to `fpm`, just add a `--`
before them, like so:
```
$ fpm run -- -f examples/obstacle.bc -t 1000 -v
```
If instead you compiled with `make`, you should have an executable
called `fstokes` in the root of the repository.
```
$ ./fstokes -f examples/obstacle.bc -t 1000 -v
```

## Using `fstokes`
A typical usage of `fstokes` looks like this:
```
$ ./fstokes -f examples/obstacle.bc -o solution.txt -t 1000 -v -p
```
Let's see all the available command line options (this info can also be found
by running `./fstokes -h`):
- `-f`, `--file`: MANDATORY, the path of the problem file       
- `-t`, `--triangles`: the number of triangles that should indicatively be used
  for triangulating the problem's domain (by default 500)       
- `-o`, `--out`: the path where the solution will be saved (by default `tmp/solution.txt`)               
- `-p`, `--plot`: if specified, the script `src/plot.py` is called, showing a plot of the solution. 
  Note: `python3`, `numpy` and `matplotlib` are required for this to work                   
- `-v`, `--verbose`: if specified, more information is printed while the program is running                
- `-h`, `--help`: displays a help message

## Input File Format
In order to solve the Stokes problem, the domain and boundary conditions
must be specified in an input file.

A few example input files are bundled with `fstokes`, in the `examples/` directory.

The format used for these input files is pretty simple, let's use `examples/obstacle.bc` as an example:
```
# this is an input file, that describes a rectangular pipe
# with a square obstacle in the middle

#    4------------------------------3
#    |>                             |>
#    |->          5----6            |->
#    |->          |    |            |->
#    |->          8----7            |->
#    |>                             |>
#    1------------------------------2

# as you can see, you can add comments by preceding them with a #

# pipe
poly
	0.0 -1.0 wall
	5.0 -1.0 out        # fluid comes out from the right side
	5.0 1.0 wall
	0.0 1.0 in 1.0      # fluid enters from the left side
endpoly

# obstacle
poly
	2.0 0.5 wall
	3.0 0.5 wall
	3.0 -0.5 wall       # these are all walls
	2.0 -0.5 wall
endpoly
```
This input file is describing a rectangular pipe, with fluid flowing from left to right,
containing a square obstacle in its middle. We can see two polygons are specified,
respectively the rectangular pipe and the square obstacle.
Inside these polygons, each line specifies the (x, y) coordinates of one point of the polygon
(the two real numbers at the beginning of the line),
followed by a description of a boundary condition.

Valid boundary conditions for an edge of the domain's boundary are
- `wall`: the fluid has velocity 0 on the edge
- `in α`: the velocity of fluid is 0 at the endpoints of the edge,
  is -α**n** in the midpoint of the edge, where **n** is the outward pointing normal,
  and is a quadratic polynomial along the edge
- `out`: the so called "natural outflow condition", 𝜕**u**/𝜕**n** - p**n** = 0 along
  the edge, where **u** is the fluid's velocity, **n** is the outward pointing normal,
  and p if the fluid's pressure.

The boundary condition described on line *i* refers to the edge with endpoints 
*i* and *i+1* (wrapping around to the first point of the polygon when we reach
the end)

Also, note that the rectangular pipe has its points listed in counterclockwise
order, while the obstacle has its points listed in clockwise order. Polygons
listed in counterclockwise order are exterior boundaries of the domain,
while polygons listed in clockwise order create holes on the domain.


## Credits
Thanks to Jonathan Shewchuk, the author of
[Triangle](https://www.cs.cmu.edu/~quake/triangle.html),
the C library that `fstokes` uses to triangulate the
problem domain.
