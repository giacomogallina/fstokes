program check
    use sparse_module
    implicit none

    type(SparseMatrix) :: A
    type(Cholesky) :: chol
    type(MinQueue) :: mq
    type(DynArrayOfInt) :: v
    real(kind=real64) :: b(5), h
    type(MinResResult) :: res
    integer(kind=int64) :: n, i

    ! call v%push(2_int64)
    ! call v%push(3_int64)
    ! call v%push(5_int64)
    ! call v%push(7_int64)

    ! print *, binary_search(v, 4_int64)

    ! call v%insert(3_int64, 4_int64)
    ! call v%print()

    n = 5_int64
    A = zeros(n, n)

    ! call A%add_at(1_int64, 1_int64, 4.0_real64)
    ! call A%add_at(1_int64, 2_int64, 12.0_real64)
    ! call A%add_at(1_int64, 3_int64, -16.0_real64)
    ! call A%add_at(2_int64, 1_int64, 12.0_real64)
    ! call A%add_at(2_int64, 2_int64, 37.0_real64)
    ! call A%add_at(2_int64, 3_int64, -43.0_real64)
    ! call A%add_at(3_int64, 1_int64, -16.0_real64)
    ! call A%add_at(3_int64, 2_int64, -43.0_real64)
    ! call A%add_at(3_int64, 3_int64, 98.0_real64)

    ! call A%add_at(1_int64, 1_int64, 4.0_real64)
    ! call A%add_at(2_int64, 2_int64, 36.0_real64)
    ! call A%add_at(3_int64, 3_int64, 25.0_real64)

    do i = 1, n
        call A%add_at(i, i, 2.0_real64)
        if (i < n) then
            call A%add_at(i, i+1, -1.0_real64)
            call A%add_at(i+1, i, -1.0_real64)
        end if
    end do

    b = 1.0

    call A%pretty_print()
    print *, b

    res = minres(n, apply_A, b, 10, 1e-6_real64, Id, Id)

    print *, res%x, res%iterations, res%converged

    chol = icholt(A, 0.5_real64)
    call chol%L%pretty_print()
    call chol%Lt%pretty_print()

    print *, chol%L%lsolve(b)
    print *, chol%Lt%usolve(chol%L%lsolve(b))

    res = minres(n, apply_A, b, 10, 1e-6_real64, P1, P2)

    print *, res%x, res%iterations, res%converged

    ! chol = A%icholt(0.0_real64)

    ! call chol%L%pretty_print()
    ! call chol%Lt%pretty_print()

    ! call mq%push(6_int64)
    ! call mq%arr%print()
    ! call mq%push(2_int64)
    ! call mq%arr%print()
    ! call mq%push(4_int64)
    ! call mq%arr%print()
    ! call mq%push(1_int64)
    ! call mq%arr%print()
    ! call mq%push(3_int64)
    ! call mq%arr%print()
    ! h = mq%pop()
    ! print *, h
    ! call mq%arr%print()
    ! h = mq%pop()
    ! print *, h
    ! call mq%arr%print()
    ! h = mq%pop()
    ! print *, h
    ! call mq%arr%print()
    ! call mq%push(5_int64)
    ! call mq%arr%print()
    ! call mq%push(2_int64)
    ! call mq%arr%print()

    print *, "Put some tests in here!"
contains
    function apply_A(x) result(y)
        real(kind=real64) :: x(:), y(size(x))
        y = A%dot(x)
    end function
    function P1(x) result(y)
        real(kind=real64) :: x(:), y(size(x))
        y = chol%L%lsolve(x)
    end function
    function P2(x) result(y)
        real(kind=real64) :: x(:), y(size(x))
        y = chol%Lt%usolve(x)
    end function
    function Id(x) result(y)
        real(kind=real64) :: x(:), y(size(x))
        y(1:n) = x(1:n)
    end function
        
end program check
