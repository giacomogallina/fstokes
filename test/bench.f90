program bench
    use iso_fortran_env
    use sparse_module
    implicit none

    type(SparseMatrix) :: A, L
    type(CompressedMatrix) :: cA, cL
    integer(kind=int64) :: n, i, j, k, rounds
    real(kind=real64) :: aij, t0, t1, rnd, time, c(3), bc(3)
    real(kind=real64), allocatable :: x(:), b(:)

    n = 100000_int64
    rounds = 50

    allocate(x(n))
    allocate(b(n))
    A = zeros(n, n)

    do i = 1, n
        do k = 1, 10
            call random_number(rnd)
            j = ceiling(rnd*n, kind=int64)
            call random_number(rnd)

            call A%add_at(i, j, rnd)
        end do
        x(i) = rnd
    end do

    cA = A%compressed()
    call A%compress()

    time = 0.0
    do i = 1, rounds
        call cpu_time(t0)
        b = A%dot(x)
        call cpu_time(t1)
        time = time + (t1 - t0) / rounds
    end do
    print *, time

    time = 0.0
    do i = 1, rounds
        call cpu_time(t0)
        call cA%dot(x, b)
        call cpu_time(t1)
        time = time + (t1 - t0) / rounds
    end do
    print *, time

    print *, norm2(A%dot(x) - b)

    L = zeros(3_int64, 3_int64)
    call L%add_at(1_int64, 1_int64, 1.0_real64)
    call L%add_at(2_int64, 1_int64, 2.0_real64)
    call L%add_at(2_int64, 2_int64, 3.0_real64)
    call L%add_at(3_int64, 1_int64, 4.0_real64)
    call L%add_at(3_int64, 2_int64, 5.0_real64)
    call L%add_at(3_int64, 3_int64, 6.0_real64)
    call L%pretty_print()
    c = 1.0
    cL = L%compressed()
    print *, cL%values
    print *, cL%columns
    print *, cL%row_starts

    print *, L%lsolve(c)
    call cL%lsolve(c, bc)
    print *, bc

end program
