module cli_module
    use iso_fortran_env
    implicit none

contains

    ! parses command line arguments for fstokes
    subroutine cli_args(input_file, n_triangles, output_file, plot, verbose)
        integer :: n_triangles, i, argc, len, max_len, iostat
        character(len=:), allocatable :: input_file, output_file
        character(len=:), allocatable :: arg
        logical :: plot, verbose, error, help

        ! default argument values
        n_triangles = 500
        input_file = ""
        output_file = "tmp/solution.txt"
        plot = .false.
        verbose = .false.
        help = .false.

        argc = command_argument_count()

        if (argc == 0) then
            help = .true.
        end if

        max_len = 32
        allocate(character(len = max_len) :: arg)

        ! try to read all arguments, but only to get their lengths
        do i = 1, argc
            call get_command_argument(number=i, value=arg, length=len)
            max_len = max(len, max_len)
        end do

        ! reallocate the arg string so that it can fit all of the arguments
        deallocate(arg)
        allocate(character(len = max_len) :: arg)

        error = .false.
        
        i = 1
        do while (i <= argc)
            ! read the i-th argument into the arg string
            call get_command_argument(number=i, value=arg)

            ! handle this argument
            if (arg == "-f" .or. arg == "--file") then
                if (i < argc) then
                    call get_command_argument(number=i+1, value=arg)
                    input_file = trim(arg)
                    i = i+1
                else
                    print *, "After the `-f` flag, a filename must follow"
                    error = .true.
                    exit
                end if
            else if (arg == "-o" .or. arg == "--out") then
                if (i < argc) then
                    call get_command_argument(number=i+1, value=arg)
                    output_file = trim(arg)
                    i = i+1
                else
                    print *, "After the `-o` flag, a filename must follow"
                    error = .true.
                    exit
                end if
            else if (arg == "-t" .or. arg == "--triangles") then
                if (i < argc) then
                    call get_command_argument(number=i+1, value=arg)
                    i = i+1
                    read(arg, *, iostat=iostat) n_triangles
                    if (iostat .ne. 0) then
                        print *, "After the `-t` flag, an integer must follow"
                        error = .true.
                        exit
                    end if
                else
                    print *, "After the `-t` flag, an integer must follow"
                    error = .true.
                    exit
                end if
            else if (arg == "-v" .or. arg == "--verbose") then
                verbose = .true.
            else if (arg == "-p" .or. arg == "--plot") then
                plot = .true.
            else if (arg == "-h" .or. arg == "--help") then
                help = .true.
                exit
            else
                print *, "Unknown option `", trim(arg), "`"
                error = .true.
                exit
            end if
            i = i+1
        end do

        if (help) then
            print *, "                                                                       "
            print *, "fstokes                                                                "
            print *, "   a simple numerical solver for the steady 2D Stokes problem,         "
            print *, "   written in Fortran 2008 by Giacomo Gallina                          "
            print *, "                                                                       "
            print *, "Usage example:                                                         "
            print *, "   fstokes -f problem_file.bc -t 1000 -p                               "
            print *, "                                                                       "
            print *, "Options:                                                               "
            print *, "   -f, --file            MANDATORY, the path of the problem file       "
            print *, "   -t, --triangles       the number of triangles that should           "
            print *, "                           indicatively be used for triangulating      "
            print *, "                           the problem's domain (by default 500)       "
            print *, "   -o, --out             the path where the solution will be saved     "
            print *, "                           (by default tmp/solution.txt)               "
            print *, "   -p, --plot            if specified, the script `src/plot.py` is     "
            print *, "                           called, showing a plot of the solution.     "
            print *, "                           Note: python3, numpy and matplotlib are     "
            print *, "                           required for this to work                   "
            print *, "   -v, --verbose         if specified, more information is printed     "
            print *, "                           while the program is running                "
            print *, "   -h, --help            displays this message                         "
            stop
        end if

        if (input_file == "") then
            print *, "The input filename must be specified"
            error = .true.
        end if

        if (error) then
            print *, "See `fstokes -h` for additional help"
            stop 1
        end if

    end subroutine
    
end module

