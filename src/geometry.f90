module geometry_module
    use iso_fortran_env

    implicit none

    real(kind=real64) :: pi = 4 * atan(1.0)

    type Point 
        real(kind=real64) :: x, y
    contains
        generic :: operator(-) => point_neg, point_diff
        procedure, private :: point_neg, point_diff
        generic :: operator(+) => point_add
        procedure, private :: point_add
        generic :: operator(*) => point_scale_left, point_scale_right, point_dot
        procedure :: point_scale_right, point_dot
        procedure, pass(self) :: point_scale_left
        procedure :: rotate90 => point_rotate90
        generic :: operator(.x.) => point_cross
        procedure, private :: point_cross
        generic :: operator(/) => point_div
        procedure, private :: point_div
        procedure :: len => point_len
        procedure :: arg => point_arg
    end type

    ! fstokes uses T2-T1 finite elements for approximating velocity and pressure.
    ! this means that for each triangle we want 6 points:
    ! the vertices a, b, c followed by the edges' midpoints ma, mb, mc
    ! where ma is the midpoint of the edge opposite of a and so on
    type Triangle
        integer(kind=int64) :: points(6)
        ! general neumann boundary conditions are currently not supported
        real(kind=real64) :: neumann(6)
    end type

    type BoundaryCondition
        logical :: is_dirichlet
        ! dirichlet boundary conditions can be degree 2 polynomials, and impose u_x and u_y along the edge ab, so they use all 6 variables
        ! neumann boundary conditions on the other hand are linear, since the pressure is linear, so they don't use m_x and m_y
        real(kind=real64) :: a_x, a_y, m_x, m_y, b_x, b_y
    contains
        procedure :: from_description
    end type

    type BoundaryConditionDescription
        character(len=30) :: name
        real(kind=real64) :: param1
    end type

    type Edge
        type(Point) :: a, b
        integer(kind=int64) :: a_idx, b_idx
        type(BoundaryCondition) :: bc
    contains
        procedure :: get_condition_at
    end type

contains
    function point_neg(self) result(res)
        class(Point), intent(in) :: self
        type(Point) :: res

        res = Point(-self%x, -self%y)
    end function

    function point_diff(self, other) result(res)
        class(Point), intent(in) :: self, other
        type(Point) :: res

        res = Point(self%x - other%x, self%y - other%y)
    end function

    function point_add(self, other) result(res)
        class(Point), intent(in) :: self, other
        type(Point) :: res

        res = Point(self%x + other%x, self%y + other%y)
    end function

    function point_scale_left(scale, self) result(res)
        class(Point), intent(in) :: self
        real(kind=real64), intent(in) :: scale
        type(Point) :: res

        res = Point(scale * self%x, scale * self%y)
    end function

    function point_scale_right(self, scale) result(res)
        class(Point), intent(in) :: self
        real(kind=real64), intent(in) :: scale
        type(Point) :: res

        res = Point(scale * self%x, scale * self%y)
    end function

    function point_dot(self, other) result(res)
        class(Point), intent(in) :: self, other
        real(kind=real64) :: res

        res = self%x * other%x + self%y * other%y
    end function

    function point_rotate90(self) result(res)
        class(Point), intent(in) :: self
        type(Point) :: res

        res = Point(-self%y, self%x)
    end function

    function point_cross(self, other) result(res)
        class(Point), intent(in) :: self, other
        real(kind=real64) :: res

        res = other * self%rotate90()
    end function

    function point_div(self, scale) result(res)
        class(Point), intent(in) :: self
        real(kind=real64), intent(in) :: scale
        type(Point) :: res

        res = Point(self%x / scale, self%y / scale)
    end function

    function point_len(self) result(res)
        class(Point), intent(in) :: self
        real(kind=real64) :: res

        res = hypot(self%x, self%y)
    end function

    function point_arg(self) result(res)
        class(Point), intent(in) :: self
        real(kind=real64) :: res

        ! yes, atan2(x, y) = arg(y + ix)
        res = atan2(self%y, self%x)
    end function
    
    subroutine from_description(self, desc, a, b)
        class(BoundaryCondition) :: self
        type(BoundaryConditionDescription) :: desc
        class(Point) :: a, b
        real(kind=real64) :: r2

        self%a_x = 0.0
        self%a_y = 0.0
        self%m_x = 0.0
        self%m_y = 0.0
        self%b_x = 0.0
        self%b_y = 0.0

        select case (desc%name)
            case ("wall")
                self%is_dirichlet = .true.
            case ("out")
                self%is_dirichlet = .false.
            case ("in")
                self%is_dirichlet = .true.

                r2 = (b%x - a%x)*(b%x - a%x) + (b%y - a%y)*(b%y - a%y)
                self%m_x = -(b%y - a%y) / sqrt(r2) * desc%param1
                self%m_y = (b%x - a%x) / sqrt(r2) * desc%param1

            case default
                error stop "unknown boundary type found"
        end select
    end subroutine

    function get_condition_at(self, p) result(res)
        class(Edge) :: self
        type(Point) :: p, res
        real(kind=real64) :: x, y, a, b, t

        x = self%b%x - self%a%x
        y = self%b%y - self%a%y
        a = self%a%x * x + self%a%y * y
        b = self%b%x * x + self%b%y * y
        t = (p%x * x + p%y * y - a) / (b - a) * 2 - 1
        
        if (self%bc%is_dirichlet) then
            res = Point(&
                self%bc%a_x * 0.5 * t * (1-t) + self%bc%m_x * (1-t) * (t+1) + self%bc%b_x * 0.5 * t * (t+1),&
                self%bc%a_y * 0.5 * t * (1-t) + self%bc%m_y * (1-t) * (t+1) + self%bc%b_y * 0.5 * t * (t+1)&
            )
        else
            res = Point(&
                self%bc%a_x * 0.5 * (1-t) + self%bc%b_x * 0.5 * (t+1),&
                self%bc%a_y * 0.5 * (1-t) + self%bc%b_y * 0.5 * (t+1)&
            )
        end if
        
    end function

    function triangle_area(a, b, c) result(res)
        type(Point) :: a, b, c
        real(kind=real64) :: res

        res = 0.5 * ( (b-a) .x. (c-a) )

        if (res < 0) then
            error stop "triangle vertices were passed in clockwise order!"
        end if
    end function

end module
