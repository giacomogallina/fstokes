#!/usr/bin/env bash
# define ELEMENT_TYPE (with type() if it's a derived type)
# define ELEMENT_NICK for a short name of the type
# optionally define ELEMENT_MODULE as the module containing ELEMENT_TYPE
# this template will create a module named dyn_array_of_element_nick_module
# (note the fact that the nickname becomes lowercase)
# containing the DynArrayOfELEMENT_NICK type and its associated methods

# thanks to https://serverfault.com/a/287690

DYN_ARRAY="DynArrayOf${ELEMENT_NICK}"

cat <<-EOF
module dyn_array_of_${ELEMENT_NICK,,}_module
    use iso_fortran_env
    $(if [ -n "$ELEMENT_MODULE" ]; then
        echo "use $ELEMENT_MODULE"
    fi)
    implicit none

    private

    type $DYN_ARRAY
        integer(kind=int64), private :: length, capacity
        $ELEMENT_TYPE, allocatable, private :: buf(:)
    contains
        procedure :: reserve => dyn_array_reserve
        procedure :: init => dyn_array_init
        procedure :: push => dyn_array_push
        procedure :: pop => dyn_array_pop
        procedure :: at => dyn_array_at
        procedure :: at_unchecked => dyn_array_at_unchecked
        procedure :: set => dyn_array_set
        procedure :: len => dyn_array_len
        procedure :: print => dyn_array_print
        procedure :: insert => dyn_array_insert
    end type

    public :: $DYN_ARRAY

contains

    subroutine dyn_array_reserve(self, capacity)
        class($DYN_ARRAY) :: self
        integer(kind=int64) :: capacity
        $ELEMENT_TYPE, allocatable :: tmp(:)

        if (.not. allocated(self%buf)) then
            allocate(self%buf(capacity))
            self%capacity = capacity
            self%length = 0
        else if (self%capacity < capacity) then
            allocate(tmp(capacity))
            tmp(1:self%capacity) = self%buf
            call move_alloc(from=tmp, to=self%buf)
            self%capacity = capacity
        end if
    end subroutine

    ! must be called before using the array!
    subroutine dyn_array_init(self)
        class($DYN_ARRAY) :: self

        call self%reserve(4_int64)
    end subroutine

    function dyn_array_at(self, idx) result(res)
        class($DYN_ARRAY) :: self
        integer(kind=int64) :: idx
        $ELEMENT_TYPE :: res

        if (idx < 1 .or. idx > self%length) then
            error stop "index out of bounds while accessing a $DYN_ARRAY"
        end if

        res = self%buf(idx)
    end function

    function dyn_array_at_unchecked(self, idx) result(res)
        class($DYN_ARRAY) :: self
        integer(kind=int64) :: idx
        $ELEMENT_TYPE :: res

        res = self%buf(idx)
    end function

    subroutine dyn_array_set(self, idx, elem)
        class($DYN_ARRAY) :: self
        integer(kind=int64) :: idx
        $ELEMENT_TYPE :: elem

        if (idx < 1 .or. idx > self%length) then
            error stop "index out of bounds while setting an element of a $DYN_ARRAY"
        end if

        self%buf(idx) = elem
    end subroutine

    subroutine dyn_array_push(self, elem)
        class($DYN_ARRAY) :: self
        $ELEMENT_TYPE :: elem

        self%length = self%length + 1

        if (self%length > self%capacity) then
            call self%reserve(2 * self%capacity)
            ! print *, "reallocating..."
        end if

        self%buf(self%length) = elem
    end subroutine

    function dyn_array_pop(self) result(res)
        class($DYN_ARRAY) :: self
        $ELEMENT_TYPE :: res

        if (self%length == 0) then
            error stop "tried to pop an empty $DYN_ARRAY"
        end if

        res = self%buf(self%length)
        self%length = self%length - 1

    end function

    function dyn_array_len(self) result(res)
        class($DYN_ARRAY) :: self
        integer(kind=int64) :: res

        res = self%length

    end function

    subroutine dyn_array_print(self)
        class($DYN_ARRAY) :: self
        integer(kind=int64) :: i

        print *, "["
        do i = 1, self%length
            print *, self%at(i)
        end do
        print *, "]"
    end subroutine

    subroutine dyn_array_insert(self, idx, elem)
        class($DYN_ARRAY) :: self
        integer(kind=int64) :: idx, i
        $ELEMENT_TYPE :: elem, last

        if (idx < 1 .or. idx > self%length + 1) then
            error stop "index out of bounds while inserting in a $DYN_ARRAY"
        end if

        call self%push(last)

        do i = self%length - 1, idx, -1
            self%buf(i+1) = self%buf(i)
        end do

        self%buf(idx) = elem

    end subroutine
end module
EOF
