
module triangulation_module
    use iso_fortran_env
    use geometry_module
    use triangle_bindings_module
    use input_module

    implicit none

    ! the structure representing the finite element mesh
    type Triangulation
        type(DynArrayOfPoint) :: points
        type(DynArrayOfTriangle) :: triangles
        ! array of indexes into points, the points where we want to find the velocity
        type(DynArrayOfInt) :: velocity_free_points
        ! array of indexes into points, the points where we already know the velocity
        ! thanks to Dirichlet boundary conditions
        type(DynArrayOfInt) :: velocity_dirichlet_points
        ! array of indexes into points, the points where we want to find the pressure
        type(DynArrayOfInt) :: pressure_points
        ! dirichlet_conditions(i) is the velocity at point velocity_dirichlet_points(i)
        type(DynArrayOfPoint) :: dirichlet_conditions
        ! the inverse of velocity_free_points, with 0 representing
        ! "not present in velocity_free_points"
        ! idx_in_velocity_free_points(i) == j <==> velocity_free_points(j) == i
        ! idx_in_velocity_free_points(i) == 0 <==> ∀ j, velocity_free_points(j) != i
        type(DynArrayOfInt) :: idx_in_velocity_free_points
        type(DynArrayOfInt) :: idx_in_velocity_dirichlet_points
        type(DynArrayOfInt) :: idx_in_pressure_points
    end type
    
contains

    function build_triangulation(prob, n_triangles, logg) result(triang)
        type(Problem) :: prob
        integer :: n_triangles
        type(Logger) :: logg
        type(Triangulation) :: triang
        type(triangulateio), target :: in, out, vorout
        integer(kind=int64) :: i
        type(Point) :: p
        type(Edge) :: e
        real(kind=c_double), allocatable, target :: in_points(:), in_holes(:)
        integer(kind=c_int), allocatable, target :: in_point_markers(:), in_segments(:), in_segment_markers(:)
        character(len=100), target :: cmd
        character(len=25) :: max_area_string
        character(len=4) :: verbose_flag
        real(kind=c_double), pointer :: out_points(:)
        integer(kind=c_int), pointer :: out_triangles(:), out_point_markers(:)
        integer(kind=int64) :: points_n, tri_n, bound_marker, bm1, bm2, a, b, c, ma, mb, mc
        real(kind=real64) :: x, y, neu_ab, neu_ac, neu_bc, neu_ba, neu_ca, neu_cb
        logical, allocatable :: is_t1_point(:)
        logical :: is_dirichlet

        call logg%start("Triangulating the problem's domain using Triangle")
        call logg%log("Preparing Triangle's inputs")

        ! points
        allocate(in_points(2*prob%points%len()))
        allocate(in_point_markers(prob%points%len()))
        do i = 1, prob%points%len()
            p = prob%points%at(i)
            in_points(2*i - 1) = p%x
            in_points(2*i) = p%y
            ! Triangle supports tagging points with an integer.
            ! we want to tag the points so that we can remember which
            ! edge they belong to, and apply the necessary boundary conditions.
            ! since each point can belong to 2 edges, we tag them with
            ! the first edge + the second edge shifted by 16 bits
            in_point_markers(i) = int(i * shift + prob%edge_before_point%at(i), c_int)
        end do
        in%pointlist = c_loc(in_points)
        in%numberofpoints = int(prob%points%len(), c_int)
        in%pointmarkerlist = c_loc(in_point_markers)

        ! edges
        allocate(in_segments(2*prob%edges%len()))
        allocate(in_segment_markers(prob%edges%len()))
        do i = 1, prob%edges%len()
            e = prob%edges%at(i)
            in_segments(2*i - 1) = int(e%a_idx, c_int)
            in_segments(2*i) = int(e%b_idx, c_int)
            in_segment_markers(i) = int(i, c_int)
        end do
        in%segmentlist = c_loc(in_segments)
        in%segmentmarkerlist = c_loc(in_segment_markers)
        in%numberofsegments = int(prob%edges%len(), c_int)

        ! holes
        allocate(in_holes(2*prob%holes%len()))
        do i = 1, prob%holes%len()
            p = prob%holes%at(i)
            in_holes(2*i - 1) = p%x
            in_holes(2*i) = p%y
        end do
        in%holelist = c_loc(in_holes)
        in%numberofholes = int(prob%holes%len(), c_int)

        ! preparing Triangle's arguments
        write(max_area_string, "(f25.17)") prob%area * 1.5 / n_triangles

        if (logg%verbose) then
            verbose_flag = "    "
        else
            verbose_flag = " -Q "
        end if
            
        write(cmd, '("-p -q30 -o2 -a", a, a, a)') adjustl(max_area_string), verbose_flag, achar(0)
        call logg%start("Calling Triangle's 'triangulate' with this options: ", trim(cmd))

        call Triangle_triangulate(c_loc(cmd), c_loc(in), c_loc(out), c_loc(vorout))


        call logg%stop()
        call logg%start("Post-processing the triangulation")

        call triang%velocity_free_points%init()
        call triang%velocity_dirichlet_points%init()
        call triang%pressure_points%init()
        call triang%dirichlet_conditions%init()
        call triang%idx_in_velocity_free_points%init()
        call triang%idx_in_velocity_dirichlet_points%init()
        call triang%idx_in_pressure_points%init()
        points_n = out%numberofpoints
        call triang%points%reserve(points_n)
        allocate(is_t1_point(points_n))
        call c_f_pointer(out%pointlist, out_points, shape = [2*points_n])
        call c_f_pointer(out%pointmarkerlist, out_point_markers, shape = [points_n])

        do i = 1, points_n
            x = out_points(2*i - 1)
            y = out_points(2*i)
            bound_marker = out_point_markers(i)
            call triang%points%push(Point(x, y))
            is_t1_point(i) = .false.
            bm1 = modulo(bound_marker, shift)
            bm2 = bound_marker / shift
            is_dirichlet = .false.
            if (bm1 > 0) then
                e = prob%edges%at(bm1)
                is_dirichlet = e%bc%is_dirichlet
            end if
            if (is_dirichlet) then
                call triang%velocity_dirichlet_points%push(i)
                call triang%dirichlet_conditions%push(e%get_condition_at(Point(x, y)))
                call triang%idx_in_velocity_free_points%push(0_int64)
                call triang%idx_in_velocity_dirichlet_points%push(triang%velocity_dirichlet_points%len())
            else if (bm2 > 0) then
                e = prob%edges%at(bm2)
                if (e%bc%is_dirichlet) then
                    call triang%velocity_dirichlet_points%push(i)
                    call triang%dirichlet_conditions%push(e%get_condition_at(Point(x, y)))
                    call triang%idx_in_velocity_free_points%push(0_int64)
                    call triang%idx_in_velocity_dirichlet_points%push(triang%velocity_dirichlet_points%len())
                else
                    call triang%velocity_free_points%push(i)
                    call triang%idx_in_velocity_free_points%push(triang%velocity_free_points%len())
                    call triang%idx_in_velocity_dirichlet_points%push(0_int64)
                end if
            else
                call triang%velocity_free_points%push(i)
                call triang%idx_in_velocity_free_points%push(triang%velocity_free_points%len())
                call triang%idx_in_velocity_dirichlet_points%push(0_int64)
            end if
        end do

        tri_n = out%numberoftriangles
        call triang%triangles%reserve(tri_n)
        call c_f_pointer(out%trianglelist, out_triangles, shape = [6*tri_n])

        do i = 1, tri_n
            a = out_triangles(6*i - 5)
            b = out_triangles(6*i - 4)
            c = out_triangles(6*i - 3)
            ma = out_triangles(6*i - 2)
            mb = out_triangles(6*i - 1)
            mc = out_triangles(6*i)
            neu_ab = 0
            neu_ac = 0
            neu_bc = 0
            neu_ba = 0
            neu_ca = 0
            neu_cb = 0
            ! TODO: add support for general neumann boundary conditions, don't just assume that they are always 0
            call triang%triangles%push(Triangle((/a, b, c, ma, mb, mc/), (/neu_ab, neu_ac, neu_bc, neu_ba, neu_ca, neu_cb/)))
            is_t1_point(a) = .true.
            is_t1_point(b) = .true.
            is_t1_point(c) = .true.
        end do

        do i = 1, points_n
            if (is_t1_point(i)) then
                call triang%pressure_points%push(i)
                call triang%idx_in_pressure_points%push(triang%pressure_points%len())
            else
                call triang%idx_in_pressure_points%push(0_int64)
            end if
        end do

        call logg%stop()
        call logg%stop()

    end function

end module
