
module compressed_matrix_module
    use iso_fortran_env

    private

    ! a sparse matrix stored in Compressed Sparse Row format,
    ! much more efficient for many operations, but not very updatable
    type :: CompressedMatrix
        real(kind=real64), allocatable :: values(:)
        integer(kind=int64), allocatable :: columns(:)
        integer(kind=int64), allocatable :: row_starts(:)
        integer(kind=int64) :: n_rows
    contains
        procedure :: dot
        procedure :: lsolve
        procedure :: usolve
    end type

    public :: CompressedMatrix

contains

    subroutine dot(self, x, y)
        class(CompressedMatrix) :: self
        real(kind=real64) :: x(:)
        real(kind=real64), intent(out) :: y(:)
        integer(kind=int64) :: i, idx

        do i = 1, self%n_rows
            y(i) = 0.0
            do idx = self%row_starts(i), self%row_starts(i+1) - 1
                y(i) = y(i) + self%values(idx) * x(self%columns(idx))
            end do
        end do
        
    end subroutine

    ! solve the lower triangular system self*x = b
    ! self must be lower triangular
    subroutine lsolve(self, b, x)
        class(CompressedMatrix) :: self
        real(kind=real64) :: b(:)
        real(kind=real64), intent(out) :: x(:)
        integer(kind=int64) :: i, idx
        real(kind=real64) :: p

        do i = 1, self%n_rows
            x(i) = 0.0
            p = 0.0
            do idx = self%row_starts(i), self%row_starts(i+1) - 2
                p = p + self%values(idx) * x(self%columns(idx))
            end do
            x(i) = (b(i) - p) / self%values(self%row_starts(i+1) - 1)
        end do
       
    end subroutine

    ! solve the upper triangular system self*x = b
    ! self must be upper triangular
    subroutine usolve(self, b, x)
        class(CompressedMatrix) :: self
        real(kind=real64) :: b(:)
        real(kind=real64), intent(out) :: x(:)
        integer(kind=int64) :: i, idx
        real(kind=real64) :: p

        do i = self%n_rows, 1, -1
            x(i) = 0.0
            p = 0.0
            do idx = self%row_starts(i) + 1, self%row_starts(i+1) - 1
                p = p + self%values(idx) * x(self%columns(idx))
            end do
            x(i) = (b(i) - p) / self%values(self%row_starts(i))
        end do
       
    end subroutine
    
end module

module sparse_matrix_module
    use sparse_vec_module
    use compressed_matrix_module
    use iso_fortran_env

    ! General purpose sparse matrix.
    ! It's stored in an uncompressed format, that permits fast random access and updates.
    ! This format, however, is not very cache-local, so "heavy" numerical operations,
    ! like dot products, would be quite slow.
    ! To remedy this, before such operations, a compressed format is calculated
    ! and cached, giving much better performance to future operations.
    type :: SparseMatrix
        integer(kind=int64) :: n_rows, n_cols
        type(SparseVec), allocatable :: rows(:)
        type(CompressedMatrix) :: compr
        logical :: compr_up_to_date
    contains
        procedure :: at
        procedure :: add_at
        procedure :: dot
        procedure :: lsolve
        procedure :: usolve
        procedure :: pretty_print
        procedure :: compressed
        procedure :: compress
    end type

contains

    ! creates a new empty matrix of the specified dimensions
    function zeros(n_rows, n_cols) result(res)
        integer(kind=int64) :: n_rows, n_cols
        type(SparseMatrix) :: res
        integer(kind=int64) :: i

        res%n_rows = n_rows
        res%n_cols = n_cols

        res%compr_up_to_date = .false.

        allocate(res%rows(n_rows))

        do i = 1, n_rows
            call res%rows(i)%init()
        end do

    end function

    ! accesses an element in O(log(nnz(self(i, :))))
    function at(self, i, j) result(res)
        class(SparseMatrix) :: self
        integer(kind=int64) :: i, j
        real(kind=real64) :: res

        res = self%rows(i)%at(j)
        
    end function

    ! adds to en element in O(log(nnz(self(i, :))))
    subroutine add_at(self, i, j, x)
        class(SparseMatrix) :: self
        integer(kind=int64) :: i, j
        real(kind=real64) :: x

        call self%rows(i)%add_at(j, x)

        self%compr_up_to_date = .false.
                    
    end subroutine

    ! scalar product with a vector in O(nnz(self))
    function dot(self, v) result(res)
        class(SparseMatrix) :: self
        real(kind=real64) :: v(:)
        real(kind=real64), allocatable :: res(:)

        allocate(res(self%n_rows))
        call self%compress()
        call self%compr%dot(v, res)

    end function

    ! solve the lower triangular system self*x = b in O(nnz(self))
    ! self must be lower triangular
    function lsolve(self, b) result(x)
        class(SparseMatrix) :: self
        real(kind=real64) :: b(:)
        real(kind=real64), allocatable :: x(:)

        allocate(x(self%n_rows))
        call self%compress()
        call self%compr%lsolve(b, x)
       
    end function

    ! solve the upper triangular system self*x = b in O(nnz(self))
    ! self must be upper triangular
    function usolve(self, b) result(x)
        class(SparseMatrix) :: self
        real(kind=real64) :: b(:)
        real(kind=real64), allocatable :: x(:)

        allocate(x(self%n_rows))
        call self%compress()
        call self%compr%usolve(b, x)
       
    end function

    subroutine pretty_print(self)
        class(SparseMatrix) :: self
        integer(kind=int64) :: i

        print *, "Sparse Matrix"
        print *, "Rows: ", self%n_rows
        print *, "Columns: ", self%n_cols

        do i = 1, self%n_rows
            print *, "row ", i
            call self%rows(i)%pretty_print()
        end do
        
    end subroutine

    ! calculates the compressed form of the matrix
    function compressed(self) result(C)
        class(SparseMatrix) :: self
        type(CompressedMatrix) :: C
        integer(kind=int64) :: i, idx, j, nnz

        C%n_rows = self%n_rows
        allocate(C%row_starts(C%n_rows + 1))

        nnz = 0
        do i = 1, self%n_rows
            nnz = nnz + self%rows(i)%values%len()
        end do

        allocate(C%values(nnz))
        allocate(C%columns(nnz))

        j = 1
        do i = 1, self%n_rows
            C%row_starts(i) = j
            do idx = 1, self%rows(i)%values%len()
                C%values(j) = self%rows(i)%values%at(idx)
                C%columns(j) = self%rows(i)%indexes%at(idx)
                j = j + 1
            end do
        end do
        C%row_starts(C%n_rows + 1) = nnz + 1
        
    end function

    ! if needed, updates the compressed form of the matrix.
    subroutine compress(self)
        class(SparseMatrix) :: self

        if (.not. self%compr_up_to_date) then
            self%compr = self%compressed()
            self%compr_up_to_date = .true.
        end if

    end subroutine
    
end module
