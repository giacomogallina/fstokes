
module min_queue_module
    use dyn_array_of_int_module
    use iso_fortran_env

    ! a priority queue, the minimun element is accessed first
    type :: MinQueue
        type(DynArrayOfInt) :: arr
    contains
        procedure :: init, push, pop, empty
    end type

contains

    ! initialization, MUST be called
    subroutine init(self)
        class(MinQueue) :: self

        call self%arr%init()

    end subroutine

    subroutine push(self, elem)
        class(MinQueue) :: self
        integer(kind = int64) :: elem, parent, idx

        call self%arr%push(elem)

        idx = self%arr%len()

        do while (idx > 1)
            parent = self%arr%at(idx / 2)
            if (elem < parent) then
                call self%arr%set(idx / 2, elem)
                call self%arr%set(idx, parent)
                idx = idx / 2
            else
                exit
            end if
        end do

    end subroutine

    function empty(self) result(res)
        class(MinQueue) :: self
        logical :: res

        res = self%arr%len() == 0

    end function

    function pop(self) result(res)
        class(MinQueue) :: self
        integer(kind = int64) :: res, idx, parent, son1, son2

        if (self%arr%len() == 0) then
            error stop "tried to pop an empty MinQueue"
        end if

        res = self%arr%at(1_int64)
        parent = self%arr%pop()
        idx = 1
        if (self%arr%len() == 0) then
            return
        end if
        call self%arr%set(idx, parent)

        do
            if (2*idx == self%arr%len()) then
                son1 = self%arr%at(2*idx)
                if (parent > son1) then
                    call self%arr%set(idx, son1)
                    call self%arr%set(2*idx, parent)
                end if
                exit
            else if (2*idx + 1 > self%arr%len()) then
                exit
            else
                son1 = self%arr%at(2*idx)
                son2 = self%arr%at(2*idx + 1)
                if (parent > son1 .and. son1 <= son2) then
                    call self%arr%set(idx, son1)
                    call self%arr%set(2*idx, parent)
                    idx = 2*idx
                else if (parent > son2 .and. son2 <= son1) then
                    call self%arr%set(idx, son2)
                    call self%arr%set(2*idx + 1, parent)
                    idx = 2*idx + 1
                else
                    exit
                end if
            end if
        end do

    end function

end module
