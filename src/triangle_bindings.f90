module triangle_bindings_module
    use iso_c_binding
    implicit none

    type, bind(c) :: triangulateio
        ! REAL *pointlist;                                               /* In / out */
        type(c_ptr) :: pointlist = c_null_ptr
        ! REAL *pointattributelist;                                      /* In / out */
        type(c_ptr) :: pointattributelist = c_null_ptr
        ! int *pointmarkerlist;                                          /* In / out */
        type(c_ptr) :: pointmarkerlist = c_null_ptr
        ! int numberofpoints;                                            /* In / out */
        integer(kind=c_int) :: numberofpoints = 0
        ! int numberofpointattributes;                                   /* In / out */
        integer(kind=c_int) :: numberofpointattributes = 0

        ! int *trianglelist;                                             /* In / out */
        type(c_ptr) :: trianglelist = c_null_ptr
        ! REAL *triangleattributelist;                                   /* In / out */
        type(c_ptr) :: triangleattributelist = c_null_ptr
        ! REAL *trianglearealist;                                         /* In only */
        type(c_ptr) :: trianglearealist = c_null_ptr
        ! int *neighborlist;                                             /* Out only */
        type(c_ptr) :: neighborlist = c_null_ptr
        ! int numberoftriangles;                                         /* In / out */
        integer(kind=c_int) :: numberoftriangles = 0
        ! int numberofcorners;                                           /* In / out */
        integer(kind=c_int) :: numberofcorners = 0
        ! int numberoftriangleattributes;                                /* In / out */
        integer(kind=c_int) :: numberoftriangleattributes = 0

        ! int *segmentlist;                                              /* In / out */
        type(c_ptr) :: segmentlist = c_null_ptr
        ! int *segmentmarkerlist;                                        /* In / out */
        type(c_ptr) :: segmentmarkerlist = c_null_ptr
        ! int numberofsegments;                                          /* In / out */
        integer(kind=c_int) :: numberofsegments = 0

        ! REAL *holelist;                        /* In / pointer to array copied out */
        type(c_ptr) :: holelist = c_null_ptr
        ! int numberofholes;                                      /* In / copied out */
        integer(kind=c_int) :: numberofholes = 0

        ! REAL *regionlist;                      /* In / pointer to array copied out */
        type(c_ptr) :: regionlist = c_null_ptr
        ! int numberofregions;                                    /* In / copied out */
        integer(kind=c_int) :: numberofregions = 0

        ! int *edgelist;                                                 /* Out only */
        type(c_ptr) :: edgelist = c_null_ptr
        ! int *edgemarkerlist;            /* Not used with Voronoi diagram; out only */
        type(c_ptr) :: edgemarkerlist = c_null_ptr
        ! REAL *normlist;                /* Used only with Voronoi diagram; out only */
        type(c_ptr) :: normlist = c_null_ptr
        ! int numberofedges;                                             /* Out only */
        integer(kind=c_int) :: numberofedges = 0
    end type

    interface
    ! /*      void triangulate(triswitches, in, out, vorout)                       */
        subroutine Triangle_triangulate(triswitches, in, out, vorout) bind(c, name = "triangulate")
            use iso_c_binding
    ! /*      char *triswitches;                                                   */
    ! /*      struct triangulateio *in;                                            */
    ! /*      struct triangulateio *out;                                           */
    ! /*      struct triangulateio *vorout;                                        */
            type(c_ptr), value :: triswitches, in, out, vorout

        end subroutine
    end interface

end module