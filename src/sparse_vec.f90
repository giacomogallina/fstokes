
module sparse_vec_module
    use dyn_array_of_int_module
    use dyn_array_of_real_module
    use iso_fortran_env

    private
    
    ! a sparse vector of real64, stores only the nonzero entries
    type :: SparseVec
        type(DynArrayOfReal) :: values
        type(DynArrayOfInt) :: indexes
    contains
        procedure :: init
        procedure :: at
        procedure :: add_at
        procedure :: dot
        procedure :: pretty_print
    end type

    public :: SparseVec

contains

    ! initializes self, MUST be called
    subroutine init(self)
        class(SparseVec) :: self

        call self%values%init()
        call self%indexes%init()
    end subroutine

    ! access in O(log(nnz(self)))
    function at(self, j) result(res)
        class(SparseVec) :: self
        integer(kind=int64) :: j
        real(kind=real64) :: res
        integer(kind=int64) :: idx

        idx = binary_search(self%indexes, j)

        if (idx < 1) then
            res = 0.0
        else
            res = self%values%at(idx)
        end if
        
    end function

    ! addition in O(nnz(self))
    subroutine add_at(self, j, x)
        class(SparseVec) :: self
        integer(kind=int64) :: j
        real(kind=real64) :: x
        integer(kind=int64) :: idx
        
        idx = binary_search(self%indexes, j)

        if (idx > 0) then
            call self%values%set(idx, self%values%at(idx) + x)
        else
            call self%values%insert(-idx, x)
            call self%indexes%insert(-idx, j)
        end if
                    
    end subroutine

    ! dot product with a dense vector in O(nnz(self))
    function dot(self, v) result(res)
        class(SparseVec) :: self
        real(kind=real64) :: v(:)
        real(kind=real64) :: res
        integer(kind=int64) :: idx

        res = 0.0
        do idx = 1, self%indexes%len()
            res = res + self%values%at_unchecked(idx) * v(self%indexes%at_unchecked(idx))
        end do

    end function

    ! arr must be increasingly ordered
    ! if idx > 0 it means that arr%at(idx) == elem
    ! if idx < 0 then arr%insert(-idx, elem) maintains arr ordered
    function binary_search(arr, elem) result(idx)
        type(DynArrayOfInt) :: arr
        integer(kind=int64) :: idx, bit
        integer(kind=int64) :: elem

        if (arr%len() == 0) then
            idx = -1
        else
            idx = 0
            do bit = bit_size(arr%len()) - leadz(arr%len()) - 1, 0, -1
                if (idx + ibset(0, bit) <= arr%len()) then
                    if (arr%at(idx + ibset(0, bit)) <= elem) then
                        idx = idx + ibset(0, bit)
                    end if
                end if
            end do
            if (idx == 0) then
                idx = -1
            else if (arr%at(idx) .ne. elem) then
                idx = -idx - 1
            end if
        end if
    end function

    subroutine pretty_print(self)
        class(SparseVec) :: self
        integer(kind=int64) :: idx
        
        do idx = 1, self%indexes%len()
            print *, self%indexes%at(idx), " -> ", self%values%at(idx)
        end do

    end subroutine

end module
