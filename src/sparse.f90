
module sparse_module
    use min_queue_module
    use sparse_matrix_module
    use iso_fortran_env

    ! the result of an incomplete Cholesky factorization
    type :: Cholesky
        type(SparseMatrix) :: L, Lt
    end type

    ! the result of MINRES
    type :: MinResResult
        real(kind=real64), allocatable :: x(:)
        integer(kind=int64) :: iterations
        logical :: converged
    end type

contains

    ! computes the incomplete Cholesky factorization with thresholding
    ! A must be symmetric and positive definite
    ! L * Lt ~ A
    function icholt(A, t) result(chol)
        type(SparseMatrix) :: A
        real(kind=real64) :: t
        type(Cholesky) :: chol
        type(SparseMatrix) :: L, Lt
        integer(kind=int64) :: n, i, j, k, idx
        real(kind=real64) :: norm, Lij
        real(kind=real64), allocatable :: r(:)
        type(MinQueue) :: nnz

        n = A%n_rows
        L = zeros(n, n)
        Lt = zeros(n, n)
        allocate(r(n))
        call nnz%init()

        do i = 1, n
            norm = 0.0
            do idx = 1, A%rows(i)%indexes%len()
                j = A%rows(i)%indexes%at(idx)
                if (j > i) then
                    exit
                end if
                r(j) = A%rows(i)%values%at(idx)
                norm = norm + r(j)*r(j)
                call nnz%push(j)
            end do
            norm = sqrt(norm)

            ! main loop
            do while (.not. nnz%empty())
                j = nnz%pop()

                if (r(j) == 0.0_real64) then
                    continue
                end if

                if (j == i) then
                    call L%add_at(i, i, sqrt(r(i)))
                    call Lt%add_at(i, i, sqrt(r(i)))
                else
                    Lij = r(j) / L%at(j, j)
                    if (abs(Lij) >= norm * t) then
                        call L%add_at(i, j, Lij)
                        call Lt%add_at(j, i, Lij)
                        do idx = 2, Lt%rows(j)%indexes%len()
                            k = Lt%rows(j)%indexes%at(idx)
                            if (r(k) == 0.0_real64) then
                                call nnz%push(k)   
                            end if
                            r(k) = r(k) - Lij * Lt%rows(j)%values%at(idx)
                        end do
                    end if
                end if

                ! cleanup r
                r(j) = 0.0

            end do

        end do

        chol%L = L
        chol%Lt = Lt

    end function

    ! iteratively solves the symmetric linear system A*x = b using MINRES
    ! n is the dimension of the system, P1 and P2 must be linear transformations
    ! such that P2 * P1 ~ A^-1
    function minres(n, A, b, maxit, tol, P1, P2) result(res)
        integer(kind=int64) :: n
        real(kind=real64) :: b(:), tol
        integer :: maxit
        interface
            function A(x) result(y)
                use iso_fortran_env
                real(kind=real64) :: x(:), y(size(x))
            end function
            function P1(x) result(y)
                use iso_fortran_env
                real(kind=real64) :: x(:), y(size(x))
            end function
            function P2(x) result(y)
                use iso_fortran_env
                real(kind=real64) :: x(:), y(size(x))
            end function
        end interface
        type(MinResResult) :: res

        integer(kind=int64) :: j
        ! underscores should be read as a minus: bj_1 means $b_(j-1)$
        real(kind=real64) :: aj, bj, bj_1, cj, dj_1, ej_2, &
                             csj, snj, csj_1, snj_1, csj_2, snj_2, &
                             r0, n2, qj, qj_1
        real(kind=real64), allocatable :: vj(:), vj_1(:), vj_2(:), Avj_1(:), xj(:), pj_1(:), pj_2(:), pj_3(:), P1b(:)

        !    __  __ _____ _   _ _____  ______  _____ 
        !   |  \/  |_   _| \ | |  __ \|  ____|/ ____|
        !   | \  / | | | |  \| | |__) | |__  | (___  
        !   | |\/| | | | | . ` |  _  /|  __|  \___ \
        !   | |  | |_| |_| |\  | | \ \| |____ ____) |
        !   |_|  |_|_____|_| \_|_|  \_\______|_____/ 
        !
        ! let's forget preconditioning for now
        ! let the krylov base be b/|b| = v0, v1, ...
        !                                             / a1 b1         \
        ! Let (v0|v1|...|vj)^t A (v0|...|vj_1) = Hj = | b1 a2 ..      |
        ! Also, let Qj * Rj = Hj , where Qj is        |    b2 .. bj_1 |
        ! orthogonal and Rj is upper triangular.      |       .. aj   |
        ! Also, since Hj is Hessemberg, Qj is the     \          bj   /
        ! product of Givens rotations:
        ! Qj = G1^t * G2^t * ... * Gj^t
        ! Let's write these Givens     /  csj snj \
        ! rotations like this:    Gj = \ -snj csj /
        ! Also, we can see that Rj has                / c1 d1 e1         \
        ! only 2 supradiagonals:                 Rj = |    c2 d2 ..      |
        ! Now, we want to minimize                    |       c3 .. ej_2 |
        ! | b - A*xj |                                |          .. dj_1 |
        ! where xj ∈ span(v0, .., vj_1)               |             cj   |
        ! Let xj = (v0|...|vj_1)*yj                   \             0    /
        ! so what we want to minimize is
        ! | b - A*(v0|...|vj_1)*yj |
        ! since this is a vector ∈ span(v0, ..., vj), it's equivalent
        ! to minimizing
        ! | (v0|...|vj)^t*b - (v0|...|vj)^t*A*(v0|...|vj_1)*yj | =
        ! = | |b|*e1 - Hj*yj | = | |b|*Gj*...*G1*e1 - Rj*yj |
        ! Note: here e1 is the vector (1, 0, ..., 0)^t
        ! Lets call |b|*Gj*...*G1*e1 =: qj. We can see that
        ! qj = |b|*(cs1  sn1*cs2  sn1*sn2*cs3  ...  sn1*..*snj_1*csj  sn1*..*snj_1*snj)^t
        ! Since the last row of Rj is 0, | b - A*xj | = |b|*sn1*..*snj_1*snj
        ! so we easily know when we have reached convergence.
        ! Now, clearly yj = Rj^-1 * qj(1:j), so
        ! xj = (v0|...|vj_1)*Rj^-1*qj(1:j)
        ! Let p0, p1, ... vectors such that
        ! (v0|...|vj_1) = Rj * (p0|...|pj_1)
        ! Note that, since Rj has a nice structure, these
        ! vectors can be easily computed:
        ! pj_1 = (vj_1 - dj_1*pj_2 - ej_2*pj_3) / cj
        ! At this point it can also be seen that
        ! xj = xj_1 + pj_1 * qj(j)
        ! so that the solution can be updated at each step
        ! and we don't need to keep all the vi and pi in memory,
        ! but only the last 2 or 3
        !
        ! For preconditioning, we take a matrix H such that
        ! H*H^t ~ A, and we use MINRES on the equivalent system
        ! H^-1*A*H^-t * z = H^-1*b
        ! where x = H^-t*z.
        ! This subroutine takes as arguments P1 and P2, that
        ! should act like H^-1 and H^-t:
        ! P1: x |-> H^-1*x     P2: x |-> H^-t*x

        allocate(vj(n))
        allocate(vj_1(n))
        allocate(vj_2(n))
        allocate(Avj_1(n))
        allocate(xj(n))
        allocate(pj_1(n))
        allocate(pj_2(n))
        allocate(pj_3(n))
        allocate(P1b(n))

        P1b = P1(b)

        j = 0
        vj = P1b / norm2(P1b)
        vj_1 = 0.0
        xj = 0.0
        pj_1 = 0.0
        pj_2 = 0.0
        r0 = norm2(P1b) !  == norm2(A*x0 - b)
        bj = 0.0
        qj_1 = 0.0
        qj = r0
        csj = 1.0
        snj = 0.0
        csj_1 = 1.0
        snj_1 = 0.0

        res%converged = .false.
        res%iterations = maxit

        do j = 1, maxit
            ! update variables
            vj_2 = vj_1
            vj_1 = vj
            pj_3 = pj_2
            pj_2 = pj_1
            bj_1 = bj
            snj_2 = snj_1
            csj_2 = csj_1
            snj_1 = snj
            csj_1 = csj
            qj_1 = qj

            ! expand Krylov subspace
            Avj_1 = P1(A(P2(vj_1)))
            aj = sum(Avj_1 * vj_1)
            vj = Avj_1 - aj * vj_1 - bj_1 * vj_2
            bj = norm2(vj)
            vj = vj / bj

            ! compute new variables
            ej_2 = bj_1*snj_2
            dj_1 = bj_1*csj_2*csj_1 + aj*snj_1
            cj = aj*csj_1 - bj_1*csj_2*snj_1

            n2 = sqrt(cj*cj + bj*bj)
            csj = cj / n2
            snj = bj / n2

            cj = cj*csj + bj*snj
            qj = - qj_1 * snj
            qj_1 = qj_1 * csj
            
            pj_1 = (vj_1 - dj_1 * pj_2 - ej_2 * pj_3) / cj

            xj = xj + pj_1 * qj_1

            ! terminate if convergence was reached
            if (abs(qj) <= r0 * tol) then
                res%converged = .true.
                res%iterations = j
                exit
            end if
            
        end do

        res%x = P2(xj)
        
    end function
    
end module
