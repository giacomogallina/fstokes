
module input_module
    use iso_fortran_env
    use geometry_module
    use logger_module
    use dyn_array_of_int_module
    use dyn_array_of_point_module
    use dyn_array_of_edge_module
    use dyn_array_of_triangle_module

    implicit none

    integer(kind=int64) :: shift = 65536

    ! the problem description
    type Problem
        type(DynArrayOfPoint) :: points
        type(DynArrayOfPoint) :: holes
        type(DynArrayOfEdge) :: edges
        ! point i is an endpoint of edges i and edge_before_point(i)
        type(DynArrayOfInt) :: edge_before_point
        real(kind=real64) :: area
    contains
        procedure :: from_file => problem_from_file
        procedure :: pretty_print
        procedure :: add_edge
        procedure :: add_hole
    end type

contains

    subroutine pretty_print(self)
        class(Problem) :: self
        integer(kind=int64) :: i
        type(Point) :: a
        type(Edge) :: e

        print *, "Points: "

        do i = 1, self%points%len()
            a = self%points%at(i)
            write(*, *) i, ": (", a%x, ", ", a%y, ")"
        end do

        print *, "Edges:"
        do i = 1, self%edges%len()
            e = self%edges%at(i)
            write(*, *) i, ": ", e%a, "---", e%b, "dir=", e%bc%is_dirichlet, &
                "(", e%bc%a_x, ", ", e%bc%a_y, ")---", &
                "(", e%bc%m_x, ", ", e%bc%m_y, ")---", &
                "(", e%bc%b_x, ", ", e%bc%b_y, ")" 
        end do

        print *, "Area:", self%area
    end subroutine

    ! given the indexes of two points and a boundary condition description,
    ! adds ad edge to the Problem and updated the domain's area
    subroutine add_edge(self, a_idx, b_idx, bc_desc)
        class(Problem) :: self
        integer(kind=int64) :: a_idx, b_idx
        type(BoundaryConditionDescription) :: bc_desc
        type(BoundaryCondition) :: bc
        type(Point) :: a, b

        a = self%points%at(a_idx)
        b = self%points%at(b_idx)
        call bc%from_description(bc_desc, a, b)
        call self%edges%push(Edge(a, b, a_idx, b_idx, bc))
        self%area = self%area + 0.5 * (a .x. b)
    end subroutine

    ! adds a hole to the Problem to indicate what is the outside of the
    ! last added polygon. in particular,
    ! the regions delimited by the Problem's edges that contain a
    ! "hole" are the outside of the domain, and everything else is inside.
    subroutine add_hole(self)
        class(Problem) :: self
        type(Point) :: a, b, c, h, ab, cb
        real(kind=real64) :: theta, r

        ! a, b, c are the last 3 points added (we assume that the last polygon had at least 3 vertices)
        ! also, polygons are described by listing their points in counterclockwise order, so the "outside"
        ! of the polygon is to the right of a-->b-->c
        a = self%points%at(self%points%len() - 2)
        b = self%points%at(self%points%len() - 1)
        c = self%points%at(self%points%len())

        ab = a - b
        cb = c - b
        theta = modulo(cb%arg() - ab%arg(), 2*pi) / 2 + ab%arg()
        r = min(cb%len(), ab%len()) / 100.0
        h = b + Point(r * cos(theta), r * sin(theta))

        call self%holes%push(h)
    end subroutine

    ! removes anything after a `#` from line, then trims
    function remove_comments(line) result(res)
        character(len=*) :: line
        character(len=:), allocatable :: res
        integer :: idx

        idx = index(line, "#")

        if (idx == 0) then
            res = trim(line)
        else
            res = trim(line(1:(idx-1)))
        end if
        
    end function
    
    ! given an open file descriptor, initializes self with the problem
    ! described in the file.
    subroutine problem_from_file(self, fd, logg)
        class(Problem) :: self
        integer :: fd ! file descriptor, must already be open
        type(Logger) :: logg
        integer :: ios
        character(len=1000) :: line
        character(len=:), allocatable :: uncommented_line
        real(kind=real64) :: x, y, param1
        character(len=30) :: bc_type
        type(BoundaryConditionDescription) :: bc_desc
        logical :: is_first_point
        integer(kind=int64) :: first_point_idx, i
        
        call logg%start("Loading problem from file...")

        ! dynarray initialization
        call self%points%init()
        call self%holes%init()
        call self%edges%init()
        call self%edge_before_point%init()
        self%area = 0.0_real64
        
        ios = 0
        ! loop over all polygons
        do

            read(fd, "(a)", iostat=ios) line

            ! if we are at the end of the file, there's nothing left to do but exit
            if (ios /= 0) then
                exit
            end if

            uncommented_line = remove_comments(line)

            ! found a polygon
            if (uncommented_line == "poly") then
                call logg%log("Found a new polygon")

                is_first_point = .true.
                do
                    read(fd, "(a)", iostat=ios) line
                    if (ios /= 0) then
                        error stop "encountered end of file before end of polygon"
                    end if
                    uncommented_line = remove_comments(line)

                    if (uncommented_line == "endpoly") then
                        call self%add_edge(self%points%len(), first_point_idx, bc_desc)
                        ! we need to add a point outside the polygon so that Triangle knows what is considered outside
                        call self%add_hole()
                        ! add the previous edge for all the points of the last polygon
                        call self%edge_before_point%push(self%points%len())
                        do i = first_point_idx, (self%points%len() - 1)
                            call self%edge_before_point%push(i)
                        end do
                        exit
                    end if

                    read(uncommented_line, *, iostat=ios) x, y, bc_type, param1
                    call self%points%push(Point(x, y))

                    if (is_first_point) then
                        is_first_point = .false.
                        first_point_idx = self%points%len()
                    else 
                        call self%add_edge(self%points%len() - 1, self%points%len(), bc_desc)
                    end if

                    bc_desc = BoundaryConditionDescription(bc_type, param1)
                end do
            end if
        end do

        call logg%log("Total area of the domain: ", self%area)
        call logg%stop()

    end subroutine

end module
