module solver_module
    use triangulation_module
    use sparse_module
    implicit none


    type Solution
        type(Point), allocatable :: velocity(:)
        real(kind=real64), allocatable :: pressure(:)
    end type

    type Integrals
        real(kind=real64) :: A(6, 6), Bx(3, 6), By(3, 6), Q(3, 3)
    end type

contains

    ! given the 3 vertices of a triangle, calculates the integrals needed for
    ! the finite element method on that triangle
    function calculate_integrals(a, b, c) result(int)
        !
        !         1
        !         a             t2 functions are quadratic polynomials that have value 1 in one of the 
        !        / |            six triangle points, and 0 in the other 5. let's call them φ1, ..., φ6
        !       /   |           
        !   6 mc     mb 5       t1 functions are linear functions that have value 1 in one of the three
        !     /       |         vertices of the triangle, and 0 in the other 2. let's call them ψ1, ψ2, ψ3
        !    /         |        
        !   b-----ma----c       we need to calculate  Aij := ∫ ∇φi : ∇φj
        ! 2       4       3                          Bxij := ∫ ψi ⋅ ∂x(φj)  
        !                                            Byij := ∫ ψi ⋅ ∂y(φj)  
        !                                             Qij := ∫ ψi ⋅ ψj
        !
        !     In order to do that, we use the fact that the integral of a polynomial of degree <= 2 on a triangle
        !     is the average of the polynomial in the 3 midpoints of the triangle, multiplied by the triangle's area
        !
        type(Point) :: a, b, c, na, nb, nc, tmpp
        type(Point) :: nabla_t2(6, 3)
        real(kind=real64) :: t1(3, 3), tmp
        type(Integrals) :: int
        real(kind=real64) :: area, darea
        integer :: i, j, k

        area = triangle_area(a, b, c)
        darea = 2*area
        na = Point( - (c%y - b%y) / darea, (c%x - b%x) / darea)
        nb = Point( - (a%y - c%y) / darea, (a%x - c%x) / darea)
        nc = Point( - (b%y - a%y) / darea, (b%x - a%x) / darea)

        ! thanks https://stackoverflow.com/a/36966254
        t1 = reshape((/ 0.0, 0.5, 0.5, &
                        0.5, 0.0, 0.5, &
                        0.5, 0.5, 0.0 /), &
                    shape(t1), order=(/2, 1/))

        nabla_t2 = reshape((/ &
            -na, na, na, &
            nb, -nb, nb, &
            nc, nc, -nc, &
            (nb+nc)*2.0_real64, nb*2.0_real64, nc*2.0_real64, &
            na*2.0_real64, (na+nc)*2.0_real64, nc*2.0_real64, &
            na*2.0_real64, nb*2.0_real64, (na+nb)*2.0_real64  &
        /), shape(nabla_t2), order=(/2, 1/))

        do i = 1, 6
            do j = 1, 6
                tmp = 0
                do k = 1, 3
                    tmp = tmp + nabla_t2(i, k) * nabla_t2(j, k)
                end do
                int%A(i, j) = tmp / 3 * area
            end do

            do j = 1, 3
                tmpp = Point(0, 0)
                do k = 1, 3
                    tmpp = tmpp + nabla_t2(i, k) * t1(j, k)
                end do
                int%Bx(j, i) = tmpp%x / 3 * area
                int%By(j, i) = tmpp%y / 3 * area
            end do
        end do

        do i = 1, 3
            do j = 1, 3
                int%Q(i, j) = sum(t1(i, 1:3) * t1(j, 1:3)) / 3 * area
            end do
        end do

    end function


    function solve_stokes(triang, logg) result(sol)
        type(Triangulation) :: triang
        type(Logger) :: logg
        type(Solution) :: sol
        integer(kind=int64) :: num_free_vel, num_pressure, t_i, i, j, i1, j1, i2, n
        real(kind=real64), allocatable :: b(:)
        type(Triangle) :: t
        type(Integrals) :: ints
        type(Point) :: bc
        type(SparseMatrix) :: A, Bx, By, Bxt, Byt, Q
        type(Cholesky) :: Achol, Qchol
        type(MinResResult) :: mr

        call logg%start("Solving Stoke's problem...")
        call logg%start("Constructing the linear system...")

        num_free_vel = triang%velocity_free_points%len()
        num_pressure = triang%pressure_points%len()

        allocate(sol%velocity(num_free_vel))
        allocate(sol%pressure(num_pressure))
        n = 2*num_free_vel + num_pressure
        allocate(b(n))

        A = zeros(num_free_vel, num_free_vel)
        Bx = zeros(num_pressure, num_free_vel)
        By = zeros(num_pressure, num_free_vel)
        Bxt = zeros(num_free_vel, num_pressure)
        Byt = zeros(num_free_vel, num_pressure)
        Q = zeros(num_pressure, num_pressure)

        b = 0.0

        ! M is of this form:    b is of this form:
        !   / A  0  Bx' \             / fx \
        !   | 0  A  By' |             | fy |
        !   \ Bx By 0   /             \ g  /
        ! see Finite Elements and Fast Iterative Solvers second edition by Elman, Sylvester & Wathen, page 130
        do t_i = 1, triang%triangles%len()
            t = triang%triangles%at(t_i)
            ints = calculate_integrals(triang%points%at(t%points(1)), triang%points%at(t%points(2)), triang%points%at(t%points(3)))
            do i = 1, 6
                i1 = triang%idx_in_velocity_free_points%at(t%points(i))
                do j = 1, 6
                    j1 = triang%idx_in_velocity_free_points%at(t%points(j))
                    if (i1 > 0) then
                        if (j1 > 0) then ! both i & j are free points => we write A
                            call A%add_at(i1, j1, ints%A(i, j))
                        else ! i is a free point, and j is a dirichlet point => we write fx and fy
                            j1 = triang%idx_in_velocity_dirichlet_points%at(t%points(j))
                            bc = triang%dirichlet_conditions%at(j1)
                            b(i1) = b(i1) - bc%x * ints%A(i, j)
                            b(num_free_vel + i1) = b(num_free_vel + i1) - bc%y * ints%A(i, j)
                        end if
                    else
                    end if
                end do
                do j = 1, 3
                    j1 = triang%idx_in_pressure_points%at(t%points(j))
                    if (i1 > 0) then ! i is a free point and j is a pressure point => we write Bx', By', Bx, By
                        call Bx%add_at(j1, i1, -ints%Bx(j, i))
                        call By%add_at(j1, i1, -ints%By(j, i))
                        call Bxt%add_at(i1, j1, -ints%Bx(j, i))
                        call Byt%add_at(i1, j1, -ints%By(j, i))
                    else ! i as a dirichlet point and j is a pressure point => we write g
                        i2 = triang%idx_in_velocity_dirichlet_points%at(t%points(i))
                        bc = triang%dirichlet_conditions%at(i2)
                        b(2*num_free_vel + j1) = b(2*num_free_vel + j1) + bc%x * ints%Bx(j, i) + bc%y * ints%By(j, i)
                    end if
                end do
            end do
            do i = 1, 3
                i1 = triang%idx_in_pressure_points%at(t%points(i))
                do j = 1, 3
                    j1 = triang%idx_in_pressure_points%at(t%points(j))
                    call Q%add_at(i1, j1, ints%Q(i, j))
                end do
            end do
        end do

        call logg%stop()
        call logg%start("Preconditioning the linear system...")

        Achol = icholt(A, 0.001_real64)
        Qchol = icholt(Q, 0.001_real64)

        call logg%stop()
        call logg%start("Solving the linear system with MINRES...")

        mr = minres(n, apply_M, b, 1000, 1e-6_real64, P1, P2)
        
        if (mr%converged) then
            call logg%log("MINRES converged in ", mr%iterations, " iterations")
        else
            call logg%log("WARNING: MINRES DID NOT CONVERGE AFTER ", mr%iterations, " ITERATIONS!")
        end if
        call logg%log("Linear system residue: ", norm2(apply_M(mr%x) - b))

        do i = 1, num_free_vel
            sol%velocity(i) = Point(mr%x(i), mr%x(num_free_vel + i))
        end do
        do i = 1, num_pressure
            sol%pressure(i) = mr%x(2*num_free_vel + i)
        end do

        call logg%stop()
        call logg%stop()

    contains

        ! example function than does nothing
        ! function Id(x) result(y)
        !     real(kind=real64) :: x(:)
        !     real(kind=real64) :: y(size(x))

        !     y(1:n) = x(1:n)
        ! end function

        function apply_M(x) result(y)
            real(kind=real64) :: x(:)
            real(kind=real64) :: y(size(x))

            y(1:num_free_vel) = A%dot(x(1:num_free_vel)) + Bxt%dot(x(2*num_free_vel + 1:n))
            y(num_free_vel+1:2*num_free_vel) = A%dot(x(num_free_vel+1:2*num_free_vel)) + Byt%dot(x(2*num_free_vel + 1:n))
            y(2*num_free_vel+1:n) = Bx%dot(x(1:num_free_vel)) + By%dot(x(num_free_vel+1:2*num_free_vel))
            
        end function

        function P1(x) result(y)
            real(kind=real64) :: x(:)
            real(kind=real64) :: y(size(x))

            y(1:num_free_vel) = Achol%L%lsolve(x(1:num_free_vel))
            y(num_free_vel+1:2*num_free_vel) = Achol%L%lsolve(x(num_free_vel+1:2*num_free_vel))
            y(2*num_free_vel+1:n) = Qchol%L%lsolve(x(2*num_free_vel+1:n))

        end function

        function P2(x) result(y)
            real(kind=real64) :: x(:)
            real(kind=real64) :: y(size(x))
            
            y(1:num_free_vel) = Achol%Lt%usolve(x(1:num_free_vel))
            y(num_free_vel+1:2*num_free_vel) = Achol%Lt%usolve(x(num_free_vel+1:2*num_free_vel))
            y(2*num_free_vel+1:n) = Qchol%Lt%usolve(x(2*num_free_vel+1:n))

        end function

    end function

    subroutine save_solution(triang, sol, output_file, logg)
        type(Triangulation) :: triang
        type(Solution) :: sol
        character(len=:), allocatable :: output_file
        type(Logger) :: logg
        type(Point) :: p
        type(Triangle) :: t
        integer(kind=int64) :: i, fd, iostat

        call logg%start("Saving the solution to '", output_file, "'...")

        open(newunit=fd, file=output_file, iostat = iostat)
        if (iostat .ne. 0) then
            print *, "Can't open '", output_file, "', quitting"
            stop 1
        end if

        write(fd, "(a)", advance="no") "points_x"
        do i = 1, triang%points%len()
            p = triang%points%at(i)
            write(fd, "(es25.17)", advance="no") p%x
        end do
        write(fd, "()")
        write(fd, "(a)", advance="no") "points_y"
        do i = 1, triang%points%len()
            p = triang%points%at(i)
            write(fd, "(es25.17)", advance="no") p%y
        end do
        write(fd, "()")

        write(fd, "(a)", advance="no") "triangles"
        do i = 1, triang%triangles%len()
            t = triang%triangles%at(i)
            write(fd, "( I20, I20, I20 )", advance="no") t%points(1) - 1, t%points(2) - 1, t%points(3) - 1
        end do
        write(fd, "()")

        write(fd, "('velocity_free_points', *(I20))") &
            (triang%velocity_free_points%at(i) - 1, i=1,triang%velocity_free_points%len())
        write(fd, "('velocity_dirichlet_points ', *(I20))") &
            (triang%velocity_dirichlet_points%at(i) - 1, i=1,triang%velocity_dirichlet_points%len())
        write(fd, "('pressure_points ', *(I20))") &
            (triang%pressure_points%at(i) - 1, i=1,triang%pressure_points%len())

        write(fd, "(a)", advance="no") "velocity_dirichlet_x"
        do i = 1, triang%dirichlet_conditions%len()
            p = triang%dirichlet_conditions%at(i)
            write(fd, "(es25.17)", advance="no") p%x
        end do
        write(fd, "()")
        write(fd, "(a)", advance="no") "velocity_dirichlet_y"
        do i = 1, triang%dirichlet_conditions%len()
            p = triang%dirichlet_conditions%at(i)
            write(fd, "(es25.17)", advance="no") p%y
        end do
        write(fd, "()")

        write(fd, "(a)", advance="no") "velocity_free_x"
        do i = 1, triang%velocity_free_points%len()
            p = sol%velocity(i)
            write(fd, "(es25.17)", advance="no") p%x
        end do
        write(fd, "()")
        write(fd, "(a)", advance="no") "velocity_free_y"
        do i = 1, triang%velocity_free_points%len()
            p = sol%velocity(i)
            write(fd, "(es25.17)", advance="no") p%y
        end do
        write(fd, "()")

        write(fd, "('pressure', *(es25.17))") sol%pressure

        close(fd)

        call logg%stop()

    end subroutine
    
end module
