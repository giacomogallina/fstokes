from matplotlib.tri import Triangulation, UniformTriRefiner, LinearTriInterpolator, CubicTriInterpolator
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import numpy as np
import sys

f = open(sys.argv[1])

points_x = [float(x) for x in f.readline().split()[1:]]
points_y = [float(x) for x in f.readline().split()[1:]]
triangles = [int(x) for x in f.readline().split()[1:]]
triangles = list(zip(*[iter(triangles)]*3))
velocity_free_points = [int(x) for x in f.readline().split()[1:]]
velocity_dirichlet_points = [int(x) for x in f.readline().split()[1:]]
pressure_points = [int(x) for x in f.readline().split()[1:]]
velocity_dirichlet_x = [float(x) for x in f.readline().split()[1:]]
velocity_dirichlet_y = [float(x) for x in f.readline().split()[1:]]
velocity_free_x = [float(x) for x in f.readline().split()[1:]]
velocity_free_y = [float(x) for x in f.readline().split()[1:]]
pressure = [float(x) for x in f.readline().split()[1:]]
    

xmin, xmax, ymin, ymax = min(points_x),max(points_x), min(points_y), max(points_y)

fig, ax = plt.subplots(2, 2)
ax[0, 0].axis("equal")
ax[1, 0].axis("equal")
ax[0, 1].axis("equal")
plt.subplots_adjust(hspace = 0.3)
ax[0, 0].set_title('Velocity')
ax[1, 0].set_title('Pressure')
ax[0, 1].set_title('Flow')

triang = Triangulation(points_x, points_y, triangles)
ax[0, 0].triplot(triang, color='0.8')

Q = ax[0, 0].quiver([points_x[i] for i in velocity_free_points] + [points_x[i] for i in velocity_dirichlet_points],
             [points_y[i] for i in velocity_free_points] + [points_y[i] for i in velocity_dirichlet_points],
             velocity_free_x + velocity_dirichlet_x, velocity_free_y + velocity_dirichlet_y,
             [0] * len(velocity_free_points) + [1] * len(velocity_dirichlet_points),
             units='width', scale=20, zorder=3, cmap = ListedColormap(["dodgerblue", "orange"]))
ax[0, 0].quiverkey(Q, 0.45, 0.9, 1, r'$1$', labelpos='E',
                   coordinates='figure')

velocity = np.zeros([len(points_x), 2])

for pi, vx, vy in zip(velocity_free_points + velocity_dirichlet_points, velocity_free_x + velocity_dirichlet_x, velocity_free_y + velocity_dirichlet_y):
    velocity[pi, :] = [vx, vy]

Y, X = np.mgrid[ymin:ymax:500j, xmin:xmax:500j]
Vx = LinearTriInterpolator(triang, velocity[:, 0])(X, Y)
Vy = LinearTriInterpolator(triang, velocity[:, 1])(X, Y)
ax[0, 1].streamplot(X, Y, Vx, Vy)#, color = np.sqrt(Vx*Vx + Vy*Vy))

ax[0, 1].triplot(triang, color = "black", alpha = 0.3, linewidth = 1)

extended_pressure = [0]*len(points_x)
for i, p in zip(pressure_points, pressure):
    extended_pressure[i] = p

con = ax[1, 0].tricontourf(triang, extended_pressure, levels = 20)
ax[1, 0].triplot(triang, color = "black", alpha = 0.3, linewidth = 1)

fig.colorbar(con, ax=ax[1, 1], location = "left")
ax[1, 1].remove()

plt.show()