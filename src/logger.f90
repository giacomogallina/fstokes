module logger_module
    use iso_fortran_env
    use dyn_array_of_real_module
    implicit none

    private

    ! A convenient data structure that handles logging to console
    ! and times the various phases of the program execution.
    ! It supports starting an activity, printing debug messages,
    ! and ending activities. Activities are automatically timed,
    ! and the time is printed when the activity ends.
    ! Also, inside activities, all messages are indented,
    ! and nested activities are supported.
    type :: Logger
        type(DynArrayOfReal) :: timers
        logical :: verbose, last_span_not_empty
    contains
        procedure :: start, stop, log, init, indent
    end type

    public :: Logger
    
contains

    ! prints arg, if present. 32 and 64 bit integers,
    ! 64 bit reals and strings are supported.
    subroutine print_generic_optional(arg)
        class(*), optional :: arg

        if (present(arg)) then
            select type(arg)
            type is (integer(kind=int64))
                write(*, "(I20)", advance="no") arg
            type is (integer(kind=int32))
                write(*, "(I10)", advance="no") arg
            type is (real(kind=real64))
                write(*, "(es20.12)", advance="no") arg
            type is (character(len=*))
                write(*, "(a)", advance="no") arg
            class default
                write(*, "(' ??? ')", advance="no")
            end select
        end if

    end subroutine

    ! prints its optional arguments, appropriately indented
    subroutine indent(self, arg1, arg2, arg3, arg4)
        class(Logger) :: self
        class(*), optional :: arg1, arg2, arg3, arg4
        integer(kind=int64) :: i

        if (self%verbose) then
            do i = 1, self%timers%len()
                write(*, "('|  ')", advance="no")
            end do
            call print_generic_optional(arg1)
            call print_generic_optional(arg2)
            call print_generic_optional(arg3)
            call print_generic_optional(arg4)
            write(*, "()")
        end if
    end subroutine

    ! MUST be called
    ! initializes the structure
    ! if verbose is true, the logger will actually print
    ! its messages, if it's false it won't print anything.
    subroutine init(self, verbose)
        class(Logger) :: self
        logical :: verbose

        call self%timers%init()
        self%verbose = verbose
        self%last_span_not_empty = .true.

    end subroutine

    ! starts an activity. a timer is started, and
    ! the following log messages will be more indented
    subroutine start(self, arg1, arg2, arg3, arg4)
        class(Logger) :: self
        class(*), optional :: arg1, arg2, arg3, arg4
        real(kind=real64) :: time

        call self%log(arg1, arg2, arg3, arg4)

        call cpu_time(time)
        call self%timers%push(time)

        self%last_span_not_empty = .false.

    end subroutine

    ! prints a log message
    subroutine log(self, arg1, arg2, arg3, arg4)
        class(Logger) :: self
        class(*), optional :: arg1, arg2, arg3, arg4

        call self%indent()
        call self%indent(arg1, arg2, arg3, arg4)
        
        self%last_span_not_empty = .true.

    end subroutine

    ! stops the most recent activity. the activity duration
    ! will be printed, and the following log messages
    ! will be less indented
    subroutine stop(self)
        class(Logger) :: self
        real(kind=real64) :: time
        integer(kind=int32) :: elapsed_ms

        call cpu_time(time)

        if (self%last_span_not_empty) then
            call self%indent()
        end if

        elapsed_ms = int(1000.0 * (time - self%timers%pop()))
        call self%indent("Done in ", elapsed_ms, " ms")

        self%last_span_not_empty = .true.

    end subroutine
    
end module
