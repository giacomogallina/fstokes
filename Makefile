
build_dir = make_build
vpath %.f90 app/ src/
vpath %.template src/
vpath %.template.sh src/
vpath %.c src/triangle/
vpath %.h src/triangle/
vpath %.o $(build_dir)
vpath %.mod $(build_dir)

FC = gfortran
CC = gcc
CFLAGS = -O2
FFLAGS = -O2 -J $(build_dir) -Wall

fstokes: $(build_dir) main.o
	$(FC) -o fstokes $(build_dir)/*.o

main.o: main.f90 cli_module.mod input_module.mod triangulation_module.mod solver_module.mod
	$(FC) $(FFLAGS) -c app/main.f90 -o $(build_dir)/main.o

triangle.o: triangle.c triangle.h
	$(CC) $(CFLAGS) -DTRILIBRARY -c src/triangle/triangle.c -o $(build_dir)/triangle.o

cli.o cli_module.mod: cli.f90
	$(FC) $(FFLAGS) -c src/cli.f90 -o $(build_dir)/cli.o

logger.o logger_module.mod: logger.f90 dyn_array_of_real_module.mod
	$(FC) $(FFLAGS) -c src/logger.f90 -o $(build_dir)/logger.o

input.o input_module.mod: input.f90 geometry_module.mod logger_module.mod dyn_array_of_point_module.mod dyn_array_of_edge_module.mod dyn_array_of_triangle_module.mod dyn_array_of_int_module.mod
	$(FC) $(FFLAGS) -c src/input.f90 -o $(build_dir)/input.o

geometry.o geometry_module.mod: geometry.f90
	$(FC) $(FFLAGS) -c src/geometry.f90 -o $(build_dir)/geometry.o

triangulation.o triangulation_module.mod: triangulation.f90 triangle_bindings_module.mod input_module.mod logger_module.mod
	$(FC) $(FFLAGS) -c src/triangulation.f90 -o $(build_dir)/triangulation.o

triangle_bindings.o triangle_bindings_module.mod: triangle_bindings.f90 triangle.o
	$(FC) $(FFLAGS) -c src/triangle_bindings.f90 -o $(build_dir)/triangle_bindings.o

solver.o solver_module.mod: solver.f90 sparse_module.mod logger_module.mod
	$(FC) $(FFLAGS) -c src/solver.f90 -o $(build_dir)/solver.o

sparse.o sparse_module.mod: sparse.f90 min_queue_module.mod sparse_matrix_module.mod
	$(FC) $(FFLAGS) -c src/sparse.f90 -o $(build_dir)/sparse.o

minqueue.o min_queue_module.mod: minqueue.f90 dyn_array_of_int_module.mod
	$(FC) $(FFLAGS) -c src/minqueue.f90 -o $(build_dir)/minqueue.o

sparse_matrix.o sparse_matrix_module.mod: sparse_matrix.f90 sparse_vec_module.mod
	$(FC) $(FFLAGS) -c src/sparse_matrix.f90 -o $(build_dir)/sparse_matrix.o

sparse_vec.o sparse_vec_module.mod: sparse_vec.f90 dyn_array_of_int_module.mod dyn_array_of_real_module.mod
	$(FC) $(FFLAGS) -c src/sparse_vec.f90 -o $(build_dir)/sparse_vec.o

dyn_array_of_point.o dyn_array_of_point_module.mod: dyn_array_of_point.f90 geometry_module.mod
	$(FC) $(FFLAGS) -c src/dyn_array_of_point.f90 -o $(build_dir)/dyn_array_of_point.o

dyn_array_of_edge.o dyn_array_of_edge_module.mod: dyn_array_of_edge.f90 geometry_module.mod
	$(FC) $(FFLAGS) -c src/dyn_array_of_edge.f90 -o $(build_dir)/dyn_array_of_edge.o

dyn_array_of_triangle.o dyn_array_of_triangle_module.mod: dyn_array_of_triangle.f90 geometry_module.mod
	$(FC) $(FFLAGS) -c src/dyn_array_of_triangle.f90 -o $(build_dir)/dyn_array_of_triangle.o

dyn_array_of_int.o dyn_array_of_int_module.mod: dyn_array_of_int.f90
	$(FC) $(FFLAGS) -c src/dyn_array_of_int.f90 -o $(build_dir)/dyn_array_of_int.o

dyn_array_of_real.o dyn_array_of_real_module.mod: dyn_array_of_real.f90
	$(FC) $(FFLAGS) -c src/dyn_array_of_real.f90 -o $(build_dir)/dyn_array_of_real.o

$(build_dir):
	mkdir -p $(build_dir)

dyn_array_of_point.f90: dynarray.template.sh
	ELEMENT_TYPE="type(Point)" ELEMENT_NICK="Point" ELEMENT_MODULE="geometry_module" src/dynarray.template.sh > src/dyn_array_of_point.f90

dyn_array_of_edge.f90: dynarray.template.sh
	ELEMENT_TYPE="type(Edge)" ELEMENT_NICK="Edge" ELEMENT_MODULE="geometry_module" src/dynarray.template.sh > src/dyn_array_of_edge.f90

dyn_array_of_triangle.f90: dynarray.template.sh
	ELEMENT_TYPE="type(Triangle)" ELEMENT_NICK="Triangle" ELEMENT_MODULE="geometry_module" src/dynarray.template.sh > src/dyn_array_of_triangle.f90

dyn_array_of_int.f90: dynarray.template.sh
	ELEMENT_TYPE="integer(kind=int64)" ELEMENT_NICK="int" src/dynarray.template.sh > src/dyn_array_of_int.f90

dyn_array_of_real.f90: dynarray.template.sh
	ELEMENT_TYPE="real(kind=real64)" ELEMENT_NICK="real" src/dynarray.template.sh > src/dyn_array_of_real.f90

.PHONY: template
template: dyn_array_of_point.f90 dyn_array_of_edge.f90 dyn_array_of_triangle.f90 dyn_array_of_int.f90 dyn_array_of_real.f90

.PHONY: clean
clean:
	rm $(build_dir)/*
