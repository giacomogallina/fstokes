program main
    use iso_fortran_env
    use cli_module
    use input_module
    use triangulation_module
    use solver_module, only: Solution, solve_stokes, save_solution
    use logger_module
    implicit none
    
    type(Logger) :: logg
    type(Problem) :: prob
    type(Triangulation) :: triang
    type(Solution) :: sol
    integer :: file, n_triangles, iostat
    character(len=:), allocatable :: input_file, output_file
    logical :: plot, verbose

    ! read command line arguments
    call cli_args(input_file, n_triangles, output_file, plot, verbose)

    ! initialise the logger
    call logg%init(verbose)

    ! try to open input file
    open(newunit=file, file=input_file, iostat = iostat, status = "old")
    if (iostat .ne. 0) then
        print *, "Can't open file '", input_file, "'"
        print *, "Quitting"
        stop
    end if

    ! read input file and build the corrisponding problem
    call prob%from_file(file, logg)
    close(file)

    ! triangulate the problem domain
    triang = build_triangulation(prob, n_triangles, logg)

    ! numerically solve Stokes problem
    sol = solve_stokes(triang, logg)

    ! save the solution
    call save_solution(triang, sol, output_file, logg)

    ! if the user requested it, call the python script
    ! that plots the solution
    if (plot) then
        call logg%log("Plotting the solution...")
        call system("python src/plot.py " // output_file)
    end if

end program main
